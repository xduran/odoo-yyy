import os
from setuptools import setup

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='odoo-yyy',
    version='1.0',
    packages=[],
    install_requires=[
    	'xlrd==0.9.3',
    ],
)

